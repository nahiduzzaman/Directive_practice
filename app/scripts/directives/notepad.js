'use strict';

/**
 * @ngdoc directive
 * @name testAppApp.directive:myDirective
 * @description
 * # myDirective
 */
angular.module('testAppApp')

.directive('notepad', function(notesFactory) {
  return {
    restrict: 'AE',
    scope: {},
    link: function(scope, elem, attrs) {
      scope.openEditor = function(index){
        scope.editMode = true;
        if (index !== undefined) {
          scope.noteText = notesFactory.get(index).content;
          scope.index = index;
        } else
          scope.noteText = undefined;
      };
      scope.save = function() {
        if (scope.noteText !== "" && scope.noteText !== undefined) {
          var note = {};
          note.title = scope.noteText.length > 10 ? scope.noteText.substring(0, 10) + '. . .' : scope.noteText;
          note.content = scope.noteText;
          note.id = scope.index != -1 ? scope.index : localStorage.length;
          scope.notes = notesFactory.put(note);
        }
        scope.restore();
      };


      scope.restore = function() {
        scope.editMode = false;
        scope.index = -1;
        scope.noteText = "";
      };

      var editor = elem.find('#editor');

      scope.restore();

      scope.notes = notesFactory.getAll();

      editor.bind('keyup keydown', function() {
        scope.noteText = editor.text().trim();
      });

    },
    templateUrl: 'views/custom.html'
  };
});


  /*.directive('myDirective', function () {
    return {
      template: '<div></div>',
      restrict: 'E',
      link: function postLink(scope, element, attrs) {
        element.text('this is the myDirective directive');
      }
    };
  })
  .directive('helloWorld', function() {
	return {
	  restrict: 'AE',
	  replace: 'true',
	  template: '<h3>Hello World!!</h3>',
	  templateUrl: 'views/custom.html',
	  link: function(scope, elem, attrs) {
      	console.log('elem',elem);
      }
	};
  })*/
  /*.directive('helloColor', function() {
	  return {
	    restrict: 'AE',
	    replace: true,
	    scope:{
	    	color: '='
	    },
	    template: '<p style="background-color:{{color}}">Hello World</p>',
	    link: function(scope, elem, attrs) {
	      elem.bind('click', function() {
	        elem.css('background-color', 'white');
	        scope.$apply(function() {
	          scope.color = "white";
	        });
	      });
	      elem.bind('mouseover', function() {
	        elem.css('cursor', 'pointer');
	      });
	      scope.$watch('color', function(val) {
                console.log('triggerd',val);
          });
	    }
	  };*/
  