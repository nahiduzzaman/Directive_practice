'use strict';

/**
 * @ngdoc function
 * @name testAppApp.controller:UserCtrl
 * @description
 * # UserCtrl
 * Controller of the testAppApp
 */
angular.module('testAppApp')
  .controller('UserCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
